
## to build/run demos

To build all demos for `neko` target, run

    bash buildsamples.sh

To build for some other target, run

    bash buildsamples.sh <target-name>

i.e.

    bash buildsamples.sh html5


## Build a specific demos

    lime build neko -Ddemo-cube
    lime run neko -Ddemo-cube

Valid inputs:

- click: stop / start animation
- arrow keys: rotate object(s)
- Q, esc: quit


![cube demo](img/MiniMightyEngine_Cube_Demo.png)

<hr>

    lime build neko -Ddemo-device
    lime run neko -Ddemo-device

Valid inputs:

- arrow keys: move camera
- Q, esc: quit

![device demo](img/MiniMightyEngine_Device_Demo.png)

<hr>


    lime build neko -Ddemo-loaders
    lime run neko -Ddemo-loaders

Valid inputs:

- click + mouse: look around
- click: stop / start animation
- arrow keys: rotate object(s)
- Q, esc: quit

![loaders demo](img/MiniMightyEngine_Loaders_Demo.png)

<hr>


    lime build neko -Ddemo-two-scenes
    lime run neko -Ddemo-two-scenes

Valid inputs:

- click: reset animation
- arrow keys: rotate camera
- Q, esc: quit

![two scenes demo](img/MiniMightyEngine_Two_Scenes_Demo.png)

<hr>


    lime build neko -Ddemo-groups-and-cameras
    lime run neko -Ddemo-groups-and-cameras

Valid inputs:

- P: pause
- C, click: switch camera
- Q, esc: quit

![groups and cameras demo](img/MiniMightyEngine_Groups_And_Cameras_Demo.png)

<hr>


    lime build neko -Ddemo-materials
    lime run neko -Ddemo-materials

Valid inputs:

- click: stop / start animation
- arrow keys: rotate object(s)
- A, W, S, D: pan camera
- Q, esc: quit

![materials demo](img/MiniMightyEngine_Materials_Demo.png)

<hr>


    lime build neko -Ddemo-materials-2
    lime run neko -Ddemo-materials-2

Valid inputs:

- click: stop / start animation
- arrow keys: rotate object(s)
- Q, esc: quit

![materials 2 demo](img/MiniMightyEngine_Materials_2_Demo.png)

<hr>


    lime build neko -Ddemo-lines-2d
    lime run neko -Ddemo-lines-2d

Valid inputs:

- Q, esc: quit

![lines 2d demo](img/MiniMightyEngine_Lines_2D_Demo.png)

<hr>


    lime build neko -Ddemo-ortho-controller
    lime run neko -Ddemo-ortho-controller

Valid inputs:

- click + mouse: move camera
- arrow keys: move camera
- R: reset camera
- A, W, S, D: move circle
- Q, esc: quit

![ortho controller demo](img/MiniMightyEngine_Ortho_Controller_Demo.png)

<hr>


    lime build neko -Ddemo-terrain
    lime run neko -Ddemo-terrain

Valid inputs:

- click + mouse: look around
- arrow keys: orbit camera
- A, W, S, D: move
- Q, esc: quit

![blender terrain demo](img/MiniMightyEngine_Terrain_Demo.png)

<hr>


    lime build neko -Ddemo-lowpolytrees
    lime run neko -Ddemo-lowpolytrees

Valid inputs:

- click + mouse: orbit camera
- R: reset camera position/rotation
- arrow keys: orbit camera
- A, W, S, D: move
- Q, esc: quit

![low poly trees demo](img/MiniMightyEngine_LowPolyTrees_Demo.png)

<hr>


    lime build neko -Ddemo-text-and-lines
    lime run neko -Ddemo-text-and-lines

Valid inputs:

- click + mouse: orbit camera
- wheel: zoom
- arrow keys: orbit camera
- R: reset camera position/rotation
- C: switch between perspective/orthographic cameras
- A, W, S, D: move
- Q, esc: quit

> NOTE: if you build and run `html5` target, you won't see the text that uses TTF font.
> In order to see it you need to generate font atlas for the font and change the code to use
> generated font atlas. See comments in sample code.

![text and lines demo](img/MiniMightyEngine_TextAndLines_Demo.png)

<hr>
