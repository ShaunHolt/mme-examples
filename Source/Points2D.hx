package;


import mme.g2d.material.Point2DMaterial;
import mme.g2d.geometry.PointCloud2D;
import mme.math.glmatrix.Vec3;
import mme.util.geometry.curve2d.Bezier4;
import mme.util.geometry.curve2d.Bezier3;
import mme.math.glmatrix.Vec2;
import mme.core.Renderer;
import mme.g2d.geometry.PolyLine2D;
import mme.camera.OrthoCamera;
import mme.core.Scene;
import lime.ui.KeyCode;
import lime.ui.KeyModifier;
import lime.ui.MouseButton;

import lime.app.Application;
import lime.graphics.RenderContext;
import lime.graphics.WebGLRenderContext;

import mme.util.geometry.AggMath;

using mme.math.glmatrix.Vec2Tools;



class Points2D extends Application {

	var scene : Scene;
	var camera : OrthoCamera;
	var renderer : Renderer;
    var pointcloud : PointCloud2D;

    private var rotationSpeed : Float;

	public function new () {
		
		super ();		
	}

    public override function onPreloadComplete() : Void {

        rotationSpeed = 45.0; // in degrees per second

		poly();
    }



	public function poly() {

		var gl = window.context.webgl;

		renderer = new Renderer(gl);

		scene = new Scene();

        var aspect = window.width / window.height;

		camera = new OrthoCamera( -1000. * aspect, 1000. * aspect, -1000., 1000., 2000. );
        camera.translate([ 0, 0, 1000 ]);

		scene.addCamera( camera );

        pointcloud = new PointCloud2D( 20.0, 20.0, 0xead446ff );
		// polyline = new PolyLine2D( 0xead446ff, 20.0 );

        // polyline.addPoint([ -500., 0. ]).addPoint([ 500., 0. ]).addPoint([ 750., 0. ]);
		// polyline.addPoint([ -500., -500. ]).addPoint([ 500., -500. ]).addPoint([ 0., 500. ]).close();
		pointcloud.addPoints([ [ -500., -500. ], [ 500., -500. ], [ 0., 500. ] ]);

		scene.add(pointcloud);

		// set black outline
        var bpointcloud = new PointCloud2D( 20.0, 20.0, new Point2DMaterial( 0x9d46eaff, 0x000000ff ) );
		// var bezierPoly = new PolyLine2D( 0x9d46eaff, 50.0, 0x000000ff );
		var qbc = new Bezier3();
        var vc = new SimpleVertexConsumer();
		qbc.bezier( vc, [700.0, 0.0], [0.0, 0.0], [0.0, 700.0] );
		for( idx in 0...vc.vertices.length) {
            // add every second point, there's quite a bit of them
            if(idx%2 == 0)
			    bpointcloud.addPoint( [vc.vertices[idx].x, vc.vertices[idx].y] );
		}
		scene.add(bpointcloud);


		// set black outline
        bpointcloud = new PointCloud2D( 20.0, 20.0, new Point2DMaterial( 0x4bea46ff, 0x000000ff ) );
		// bezierPoly = new PolyLine2D( 0x4bea46ff, 20.0, 0x000000ff );
		var cbc = new Bezier4();
        var vc = new SimpleVertexConsumer();
        cbc.bezier( vc, [700.0, 0.0], [0.0, 0.0], [0.0, 700.0], [-700.0, 700.0] );
		for( idx in 0...vc.vertices.length) {
            // add every third point, there's quite a bit of them
            if(idx%3 == 0)
			    bpointcloud.addPoint( [vc.vertices[idx].x, vc.vertices[idx].y] );
		}
		scene.add(bpointcloud);
		bpointcloud.translate([ 0.0, -100.0, 0.0 ]);
		
	}

    public override function update( deltaTime : Int ) : Void {

        if( !preloader.complete ) return;

		// deltaTime is in milliseconds
		var delta = deltaTime / 1000.0;
		var angle = delta * rotationSpeed;

		pointcloud.rotate( angle, Vec3.Z_AXIS );
	}

	public override function render (context:RenderContext):Void {

		if (!preloader.complete) return;
		
		switch (context.type) {
			
			case OPENGL, OPENGLES, WEBGL:

				var gl = context.webgl;
				renderer.render( gl, scene );

			default:
		}	
	}

    public override function onMouseDown( x : Float, y : Float, button : MouseButton ) : Void {
    }

    public override function onKeyDown( keyCode : KeyCode, modifier : KeyModifier ) : Void {

        switch( keyCode ) {

            case KeyCode.ESCAPE | KeyCode.Q:
                window.close();
            
            default:
        }
        
    }	
}