package;

import lime.utils.Assets;
import lime.app.Application;
import lime.utils.Log;

import lime.graphics.RenderContext;
import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

import lime.ui.MouseButton;
import lime.ui.KeyCode;
import lime.ui.KeyModifier;


import mme.core.Renderer;
import mme.core.Scene;
import mme.core.Camera;

import mme.material.TextureMaterial;
import mme.material.BasicMaterial;

import mme.geometry.Triangle;
import mme.geometry.Rectangle;
import mme.geometry.Square;

import mme.camera.OrthoCamera;


class Device extends Application {

    private var renderer : Renderer;

    private var square : Square;
    private var rectangle : Rectangle;
    private var triangle : Triangle;

    private var camera : Camera;

    private var scene : Scene;

    public function new() {

        super();

        trace("Mini-Mighty-Engine v0.1 - Device demo");
        Log.info("Mini-Mighty-Engine v0.1 - Device demo");
    }

    public override function onRenderContextLost() : Void {
        trace("onRenderContextLost");
    }

    public override function onRenderContextRestored( context:RenderContext ) : Void {
        trace("onRenderContextRestored");
    }

    public override function onPreloadComplete() : Void {
        trace("onPreloadComplete");

        init();
    }

    private function init() : Void {

        initApp();


        switch( window.context.type ) {

            case CAIRO:
                Log.error("Render context not supported: CAIRO");
                window.close();

            case CANVAS:
                Log.error("Render context not supported: CANVAS");
                window.close();

            case DOM:
                Log.error("Render context not supported: DOM");
                window.close();

            case FLASH:
                Log.error("Render context not supported: FLASH");
                window.close();

            case OPENGL, OPENGLES, WEBGL:

            #if ( lime_opengl || lime_opengles )
                trace( "OpenGL version: " + GL.getString( GL.VERSION ) );
                trace( "GLSL version: " + GL.getString( GL.SHADING_LANGUAGE_VERSION ) );
            #end

                var gl = window.context.webgl;

                renderer = new Renderer( gl );

                initScene( gl );

            default:

                Log.error("Current render context not supported by this sample");
                window.close();

        }
    }

    private function initApp() : Void {

    }

    private function createCamera() : Camera {

        var camera : Camera;

        var left:Float = -1000;
        var right:Float = 1000;
        var bottom:Float = -1000;
        var top:Float = 1000;
        var depth:Float = 1000;

        var aspect = window.width / window.height;

        camera = new OrthoCamera( left, right * aspect, bottom, top, depth );
        // Flip Y-axis
        // camera = new OrthoCamera( left, right, top, bottom, depth  );
        // Flip Z-axis
        // camera = new OrthoCamera( left, right, bottom, top, depth );
        camera.translate([0,0,depth/2]);

        return camera;
    }

    private function initScene( gl : WebGLRenderContext ) : Void {

		//
		// SCENE
		//

        scene = new Scene();

        camera = createCamera();
        scene.addCamera( camera );

        var black = new BasicMaterial( 0x000000FF );
        var orange = new BasicMaterial( 0xFF7F00FF );
        var distanceA = 550.;
        var distanceB = 200.;

		//
		// SQUARE
		//
        square = new Square( 300., new TextureMaterial( Assets.getImage("assets/UV_Grid_1024.jpeg") ) );

        var square_rectangle_connector = new Rectangle( 5., distanceA, orange );
        square_rectangle_connector.mesh.moveTo( [ 0., distanceA / 2., 5.0 ] );
        square.add( square_rectangle_connector );

        // place small dot at the object origin, a.k.a. center of rotation
        var dot = new Square( 5., black );
        dot.mesh.moveTo( [ 0., 0., 10. ] );
        square_rectangle_connector.add( dot );

        scene.add( square );


		//
		// RECTANGLE
		//
        rectangle = new Rectangle( 100., 200., new BasicMaterial() );
        rectangle.translate( [ 0., distanceA, 0.0 ] );

        var rectangle_triangle_connector = new Rectangle( distanceB, 5., orange );
        rectangle_triangle_connector.mesh.moveTo( [ distanceB / 2., 0., 5.0 ] );
        rectangle.add( rectangle_triangle_connector );

        // place small dot at the object origin, a.k.a. center of rotation
        dot = new Square( 5., black );
        dot.mesh.moveTo( [ 0., 0., 10. ] );
        rectangle.add( dot );

        square.add( rectangle );


		//
		// TRIANGLE
		//
        triangle = new Triangle(
            // vertex 0
            [ -100., -100., 0.0 ],
            // vertex 1
            [ -100., 100., 0.0 ],
            // vertex 2
            [ 100., -100., 0.0 ],
            // lime color
            new BasicMaterial( 0x6fac17ff )
        );
        triangle.translate( [ distanceB, 0.0, 0.0 ] );

        // place small dot at the object origin, a.k.a. center of rotation
        dot = new Square( 5., black );
        dot.mesh.moveTo( [ 0., 0., 10. ] );
        triangle.add( dot );

        rectangle.add( triangle );
    }

    public override function update( deltaTime : Int ) : Void {

        if( !preloader.complete ) return;

        // deltaTime is in milliseconds
        var rotationSpeed = 90.0; // in degrees per second
        var delta = deltaTime / 1000.0;

        square.rotate( delta * rotationSpeed );
        rectangle.rotate( delta * rotationSpeed );
        triangle.rotate( delta * rotationSpeed );
    }

    public override function onMouseDown( x : Float, y : Float, button : MouseButton ) : Void {

    }

    public override function onKeyDown( keyCode : KeyCode, modifier : KeyModifier ) : Void {

        var nudge = 50.0;

        switch( keyCode ) {

            case KeyCode.ESCAPE | KeyCode.Q:
                window.close();

            case KeyCode.UP:
                camera.translate([ 0.0, - nudge, 0.0 ]);

            case KeyCode.DOWN:
                camera.translate([ 0.0, nudge, 0.0 ]);

            case KeyCode.RIGHT:
                camera.translate([ - nudge, 0.0, 0.0 ]);

            case KeyCode.LEFT:
                camera.translate([ nudge, 0.0, 0.0 ]);
            
            default:
        }
        
    }

    public override function render( context : RenderContext ) : Void {

        if( !preloader.complete ) return;

        switch( context.type ) {

            case OPENGL, OPENGLES, WEBGL:

                var gl = context.webgl;

                renderer.render( gl, scene );
            default:

        }
    }
}
