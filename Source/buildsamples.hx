class Buildsamples extends hxp.Script {

    public function new () {

        super ();

        // trace (command);
        // trace (commandArgs);
        // trace (flags.keys ());
        // trace (defines.keys ());
        // trace (options.keys ());

        var demoNames = [
            // "demo",
            "demo-cube",
            "demo-device",
            "demo-loaders",
            "demo-two-scenes",
            "demo-groups-and-cameras",
            "demo-materials",
            "demo-materials-2",
            "demo-lines-2d",
            "demo-points-2d",
            "demo-ortho-controller",
        ];

        for( name in demoNames ) {

            hxp.System.runCommand(".", "lime", ["build", "neko", "-D" + name], true, false, true);
        }
    }
}