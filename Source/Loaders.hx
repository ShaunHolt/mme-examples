package;

import mme.math.glmatrix.Vec3;
import lime.utils.Assets;
import lime.app.Application;
import lime.utils.Log;

import lime.graphics.RenderContext;
import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

import lime.ui.MouseButton;
import lime.ui.KeyCode;
import lime.ui.KeyModifier;


import mme.core.Renderer;
import mme.core.Mesh;
import mme.core.Scene;
import mme.core.Camera;

import mme.material.PhongMaterial;

import mme.geometry.MeshGeometry;

import mme.camera.PerspectiveCamera;

import mme.light.AmbientLight;
import mme.light.PointLight;

import mme.util.loader.WaveObjLoader;
import mme.util.loader.StlLoader;


class Loaders extends Application {

    private var renderer : Renderer;

    private var objMeshGeometry_1 : MeshGeometry;
    private var objMeshGeometry_2 : MeshGeometry;
    private var stlMeshGeometry : MeshGeometry;

    private var camera : Camera;

    private var scene : Scene;

    private var rotationActive : Bool;

    public function new() {

        super();

        trace("Mini-Mighty-Engine v0.1 - OBJ, STL Loaders demo");
        Log.info("Mini-Mighty-Engine v0.1 - OBJ, STL Loaders demo");
    }

    public override function onRenderContextLost() : Void {
        trace("onRenderContextLost");
    }

    public override function onRenderContextRestored( context:RenderContext ) : Void {
        trace("onRenderContextRestored");
    }

    public override function onPreloadComplete() : Void {
        trace("onPreloadComplete");

        init();
    }

    private function init() : Void {

        initApp();


        switch( window.context.type ) {

            case CAIRO:
                Log.error("Render context not supported: CAIRO");
                window.close();

            case CANVAS:
                Log.error("Render context not supported: CANVAS");
                window.close();

            case DOM:
                Log.error("Render context not supported: DOM");
                window.close();

            case FLASH:
                Log.error("Render context not supported: FLASH");
                window.close();

            case OPENGL, OPENGLES, WEBGL:

            #if ( lime_opengl || lime_opengles )
                trace( "OpenGL version: " + GL.getString( GL.VERSION ) );
                trace( "GLSL version: " + GL.getString( GL.SHADING_LANGUAGE_VERSION ) );
            #end

                var gl = window.context.webgl;

                renderer = new Renderer( gl );

                initScene( gl );

            default:

                Log.error("Current render context not supported by this sample");
                window.close();

        }
    }

    private function initApp() : Void {

        rotationActive = true;
    }

    private function initScene( gl : WebGLRenderContext ) : Void {

		//
		// SCENE
		//

        scene = new Scene();

        var aspect = window.width / window.height;

        var camera = new PerspectiveCamera( 60., aspect, 1., 1000. );
        camera.translate( [ 0., 0., 980. ] );

        scene.addCamera( camera );

        var phongGreen = new PhongMaterial( 0x00ff00ff );
        var phongOrange = new PhongMaterial( 0xfc9b0aff );
        var phongPurple = new PhongMaterial( 0xd80afcff );

        var distanceA = 550.;
        var distanceB = 200.;

        var ambientLight = new AmbientLight( 0xffffffff, 0.1 );
        scene.addLight( ambientLight );

        var pointLight = new PointLight( 0xffffffff, 1.0 );
        pointLight.translate( [ 100.0, 0.0, 700.0 ] );
        scene.addLight( pointLight );


        var objLoader = new WaveObjLoader();
        var objMesh : Mesh;


        objMesh = objLoader.loadAsMesh( "assets/models/cuboid.obj", Assets.getBytes );
        objMesh.scaleTo( 50., 50., 50. );

        objMeshGeometry_1 = new MeshGeometry( objMesh, phongGreen );
        objMeshGeometry_1.translate( [ 75., 50., 560. ] );
        objMeshGeometry_1.rotate( 45, Vec3.Y_AXIS );
        objMeshGeometry_1.rotate( 30, Vec3.X_AXIS );
        scene.add( objMeshGeometry_1 );


        objMesh = objLoader.loadAsMesh( "assets/models/icosahedron.obj", Assets.getBytes );
        objMesh.scaleTo( 50., 50., 50. );

        objMeshGeometry_2 = new MeshGeometry( objMesh, phongOrange );
        objMeshGeometry_2.translate( [ -75., 50., 560. ] );
        objMeshGeometry_2.rotate( 45, Vec3.Y_AXIS );
        objMeshGeometry_2.rotate( 30, Vec3.X_AXIS );
        scene.add( objMeshGeometry_2 );


        var stlLoader = new StlLoader();
        var stlMesh = stlLoader.load( 'assets/models/hole.stl' );
        stlMesh.scaleTo( 2., 2., 2. );

        stlMeshGeometry = new MeshGeometry( stlMesh, phongPurple );
        stlMeshGeometry.translate( [ 0., -100., 560. ] );
        scene.add( stlMeshGeometry );
    }

    public override function update( deltaTime : Int ) : Void {

        if( !preloader.complete ) return;

        if( rotationActive ) {

            // deltaTime is in milliseconds
            var rotationSpeed = 90.0; // in degrees per second
            var delta = deltaTime / 1000.0;

            objMeshGeometry_1.rotate( delta * rotationSpeed, Vec3.Y_AXIS );
            objMeshGeometry_2.rotate( delta * rotationSpeed, Vec3.X_AXIS );
            stlMeshGeometry.rotate( delta * rotationSpeed, Vec3.Y_AXIS );
            stlMeshGeometry.rotate( delta * rotationSpeed * 2, Vec3.X_AXIS );
        }
    }

    public override function onMouseDown( x : Float, y : Float, button : MouseButton ) : Void {

        rotationActive = ! rotationActive;
    }

    public override function onKeyDown( keyCode : KeyCode, modifier : KeyModifier ) : Void {

        var rotation = 5.0; // in degrees

        switch( keyCode ) {

            case KeyCode.ESCAPE | KeyCode.Q:
                window.close();

            case KeyCode.UP:
                objMeshGeometry_1.rotate( - rotation, Vec3.X_AXIS );
                objMeshGeometry_2.rotate( - rotation, Vec3.X_AXIS );
                stlMeshGeometry.rotate( - rotation, Vec3.X_AXIS );

            case KeyCode.DOWN:
                objMeshGeometry_1.rotate( rotation, Vec3.X_AXIS );
                objMeshGeometry_2.rotate( rotation, Vec3.X_AXIS );
                stlMeshGeometry.rotate( rotation, Vec3.X_AXIS );

            case KeyCode.RIGHT:
                objMeshGeometry_1.rotate( rotation, Vec3.Y_AXIS );
                objMeshGeometry_2.rotate( rotation, Vec3.Y_AXIS );
                stlMeshGeometry.rotate( rotation, Vec3.Y_AXIS );

            case KeyCode.LEFT:
                objMeshGeometry_1.rotate( - rotation, Vec3.Y_AXIS );
                objMeshGeometry_2.rotate( - rotation, Vec3.Y_AXIS );
                stlMeshGeometry.rotate( - rotation, Vec3.Y_AXIS );
            
            default:
        }
        
    }

    public override function render( context : RenderContext ) : Void {

        if( !preloader.complete ) return;

        switch( context.type ) {

            case OPENGL, OPENGLES, WEBGL:

                var gl = context.webgl;

                renderer.render( gl, scene );
            default:

        }
    }
}
