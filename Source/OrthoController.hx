package;


import lime.utils.Assets;
import lime.graphics.opengl.GL;
import lime.ui.MouseWheelMode;
import mme.material.TextureMaterial;
import mme.geometry.Square;
import mme.g2d.material.Point2DMaterial;
import mme.core.Group;
import mme.core.Renderer;
import mme.g2d.geometry.PolyLine2D;
import mme.g2d.geometry.PointCloud2D;
import mme.camera.OrthoCamera;
import mme.core.Scene;
import lime.ui.KeyCode;
import lime.ui.KeyModifier;
import lime.ui.MouseButton;

import lime.app.Application;
import lime.graphics.RenderContext;

import mme.extras.camera.OrthoCameraController;

class OrthoController extends Application {

	var scene : Scene;
	var camera : OrthoCamera;
	var renderer : Renderer;
	var polyline : PolyLine2D;
    var curves : Group;

    var cameraController : OrthoCameraController;

    public function new() {

        super();

        trace("Mini-Mighty-Engine - OrthoCameraController");
    }

    public override function onPreloadComplete() : Void {
        trace("onPreloadComplete");
    #if ( lime_opengl || lime_opengles )
        trace( "OpenGL version: " + GL.getString( GL.VERSION ) );
        trace( "GLSL version: " + GL.getString( GL.SHADING_LANGUAGE_VERSION ) );
    #end

        doLineStuff();
    }


    private function doLineStuff() {

		var gl = window.context.webgl;

		renderer = new Renderer( gl );
        renderer.viewport( gl, window.width, window.height );

		scene = new Scene();

        // OrthoCameraController will set camera size to window size
		camera = new OrthoCamera(
            0.0, 100, // left, right
            0.0, 100, // bottom, top
            2000. ); // depth
        camera.translate([ 0, 0, 1000 ]);

		scene.addCamera( camera );

        cameraController = new OrthoCameraController( camera, window );



        var lineWidth = 8.0;
		var bbox = new PolyLine2D( 0xff0000ff, lineWidth );

        var halfLineWidth = lineWidth / 2.0;
        var boxWidth = cameraController.stageWidth;
        var boxHeight = cameraController.stageHeight;
		bbox.addPoints([
            [ halfLineWidth, halfLineWidth ],
            [ boxWidth - halfLineWidth, halfLineWidth ], 
            [ boxWidth - halfLineWidth, boxHeight - halfLineWidth ],
            [ halfLineWidth, boxHeight - halfLineWidth ]
        ]).close();

		scene.add( bbox );

        curves = new Group();
        scene.add( curves );


        var point_material = new Point2DMaterial( 0xff00ffff, 0x000000ff );
        var le_point = new PointCloud2D( 100., 100., point_material );
        le_point.addPoint([0,0]);
        le_point.translate([ 300.,300.,0. ]);
        curves.add(le_point);


        var square = new Square( 300., new TextureMaterial(Assets.getImage("assets/UV_Grid_1024.jpeg")) );
        square.translate([ 300., 300., -10. ]);
        scene.add(square);

    }

    public override function onKeyDown( keyCode : KeyCode, modifier : KeyModifier ) : Void {

        switch( keyCode ) {

            case KeyCode.ESCAPE | KeyCode.Q:
                window.close();

            case KeyCode.R:
                cameraController.updateCameraReset();

            case KeyCode.UP:
                camera.translateY( -10.0 );

            case KeyCode.DOWN:
                camera.translateY( 10.0 );

            case KeyCode.LEFT:
                camera.translateX( 10.0 );

            case KeyCode.RIGHT:
                camera.translateX( -10.0 );


            case KeyCode.W:
                curves.translateY( 10.0 );

            case KeyCode.S:
                curves.translateY( -10.0 );

            case KeyCode.A:
                curves.translateX( -10.0 );

            case KeyCode.D:
                curves.translateX( 10.0 );
            

            
            default:
        }
        
    }

	public override function render (context:RenderContext):Void {

		if (!preloader.complete) return;
		
		switch (context.type) {
			
			case OPENGL, OPENGLES, WEBGL:

				var gl = context.webgl;
				renderer.render( gl, scene );

			default:
		}	
	}
}
